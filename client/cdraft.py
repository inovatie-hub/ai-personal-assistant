from __future__ import print_function
from tkinter import *
from tkinter.scrolledtext import *
import os
from gtts import gTTS
import speech_recognition as sr
import re
from cryptography.fernet import Fernet
import shlex
import requests
from subprocess import PIPE
import subprocess
import json
from youtube_search import YoutubeSearch
import webbrowser
from youtube_search import YoutubeSearch
import smtplib
import pyttsx3
import pythoncom
import requests
import webbrowser
import socket
from pyautogui import press, typewrite, hotkey
from datetime import datetime, timedelta
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

SCOPES = ['https://www.googleapis.com/auth/calendar']
key = 'GCOKn9PQow4ZimsU0aBGZZQHIi_lTq7o2UmxLpK-wfI='.encode()
cipher_suite = Fernet(key)

def convert24(str1): 
    try:
        if str1[-4:] == "a.m." and str1[:-8] == "12": 
            return int(00) , int(str1[-7:-5]) 
        elif str1[-4:] == "a.m.": 
            return int(str1[:-8]) , int(str1[-7:-5]) 
        elif str1[-4:] == "p.m." and str1[:-8] == "12": 
            return int(str1[:-8]) , int(str1[-7:-5]) 
        else: 
            return int(str1[:-8]) + 12 , int(str1[-7:-5])
    except:
        botchat('cannot understand time, adding event to 9 am')
        return 9,0
   
def execute_and_return(cmd):
    args = shlex.split(cmd)
    proc = subprocess.Popen(args, stdout=PIPE, stderr=PIPE)
    out, err = proc.communicate()
    return out, err

def make_request(error):
    print("Searching for "+error)
    resp  = requests.get("https://api.stackexchange.com/"+"2.2/search?order=desc&tagged=python&sort=activity&intitle={}&site=stackoverflow".format(error))
    return resp.json()

def get_urls(json_dict):
    url_list = []
    count = 0
    for i in json_dict['items']:
        if i["is_answered"]:
            url_list.append(i["link"])
        count+=1
        if count == len(i) or count == 3:
            break
    import webbrowser
    for i in url_list:
        webbrowser.open(i)

def createevent(eventname,s,eventtime):
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    #s = "26 april 2020"
    print(s)
    d = datetime.strptime(s,'%d %B %Y')
    #eventname = 'next event'
    h,m = convert24(eventtime)
    print(h,m) 
    service = build('calendar', 'v3', credentials=creds)
    try:
        start_time = datetime(int(d.strftime('%Y'))*1, int(d.strftime('%m'))*1, int(d.strftime('%d'))*1, h, m, 0)
        end_time = start_time + timedelta(hours=4)
        timezone = 'Asia/Kolkata'
        calendar_id='primary'
        event = {
          'summary': eventname,
          'description': 'new event created by bot',
          'start': {
            'dateTime': start_time.strftime("%Y-%m-%dT%H:%M:%S"),
            'timeZone': timezone,
          },
          'end': {
            'dateTime': end_time.strftime("%Y-%m-%dT%H:%M:%S"),
            'timeZone': timezone,
          },
          'reminders': {
            'useDefault': False,
            'overrides': [
              {'method': 'email', 'minutes': 24 * 60},
              {'method': 'popup', 'minutes': 10},
            ],
          },
        }

        service.events().insert(calendarId=calendar_id, body=event).execute()
        resp = 'Event added successfully'
    except:
        resp = 'Cannot add Event, try again'
    return resp

def talkToMe(audio):
    import pythoncom
    pythoncom.CoInitialize()
    engine = pyttsx3.init()
    engine.setProperty('voice', 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-US_ZIRA_11.0')
    engine.say(audio)
    engine.runAndWait()

def searchquery(command):
    reg_ex = re.search('search (.+)', command)
    if reg_ex:
        text = reg_ex.group(1)
        botchat('searching '+text)
        url = 'https://www.google.com/search?&q='+text
        webbrowser.open(url)

def youtubequery(command):
    botchat("which video you want me to play")
    search = othercmd()
    usertemp(search)
    results = YoutubeSearch(search, max_results=10).to_json()
    result_dict = json.loads(results)
    print(result_dict['videos'][0]['link'])
    webbrowser.open('https://www.youtube.com'+result_dict['videos'][0]['link'])
    botchat("playing "+search+" on youtube")

def myCommand():
    "listens for commands"

    r = sr.Recognizer()

    with sr.Microphone() as source:
        print('Ready...')
        r.pause_threshold = 1
        r.dynamic_energy_threshold = False
        r.adjust_for_ambient_noise(source, duration=0.5)
        audio = r.listen(source,phrase_time_limit=5)


    try:
        command = r.recognize_google(audio).lower()
        print('You said: ' + command + '\n')
        if 'add event' in command:
            addevent(command)
        elif 'search' in command:
            usertemp(command)
            searchquery(command)
        elif 'youtube' in command:
            usertemp(command)
            youtubequery(command)
        elif 'email' in command:
            talkToMe('Who is the recipient?')
            recipient = myCommand()
            if 'kaushik' in recipient:
                talkToMe('What should I say?')
                content = myCommand()
                mail = smtplib.SMTP('smtp.gmail.com', 587)
                mail.ehlo()
                mail.starttls()
                mail.login('mgkamalesh7', 'edenkamal1029')
                mail.sendmail('kaushik s', 'skaushikv3@gmail.com', content)
                mail.close()
                talkToMe('Email sent.')
                bottext = 'email sent'
            else:
                talkToMe('I don\'t know what you mean!')
        elif 'compile' in command:
            usertemp(command)
            reg_ex = re.search('compile (.+)', command)
            if reg_ex:
                text = reg_ex.group(1)
            out, err = execute_and_return("python "+text+".py")
            error_message = err.decode("utf-8").strip().split("\r\n")[-1]
            botchat(error_message)
            if error_message:
                json = make_request(error_message)
                get_urls(json)
                json = make_request(error_message.split(':')[0])
                get_urls(json)
                botchat("opening url")
            else:
                botchat("No errors in file")
        else:
            userchat(command)
        #

    #loop back to continue to listen for commands if unrecognizable speech is received
    except sr.UnknownValueError:
        #talkToMe('Your last command couldn\'t be heard')
        print('Your last command couldn\'t be heard')
        command = myCommand();

    #user.append(command)
    return command

def othercmd():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print('Ready...')
        r.pause_threshold = 1
        r.dynamic_energy_threshold = False
        r.adjust_for_ambient_noise(source, duration=0.5)
        audio = r.listen(source,phrase_time_limit=5)

    try:
        command = r.recognize_google(audio).lower()
        print('You said: ' + command + '\n')
        usertemp(command)
 
    except sr.UnknownValueError:
        talkToMe('Your last command couldn\'t be heard')
        print('Your last command couldn\'t be heard')
        command = myCommand();
    return command


root = Tk()

root.title('Intelligent Product Master')
root.geometry("400x500+950+200") 
root.resizable(0, 0) 
root.configure(background="snow")

TextBox = ScrolledText(root,width=55, height=30, wrap=WORD)
TextBox.grid(column=0, row=2, columnspan=2, sticky='nesw', padx=3, pady=3) 

def userchat(txt):
    TextBox.insert(END, "User", 'user')
    TextBox.insert(END, "\t")
    TextBox.insert(END, txt,'userval')
    TextBox.tag_config('user', foreground='black',background="orange",font=("Helvetica", 18))
    TextBox.tag_config('userval', foreground='black',font=("Helvetica", 16))
    TextBox.insert(END, "\n")
    TextBox.yview(END)
    TextBox.pack()
    root.update()
    encoded_text = cipher_suite.encrypt(txt.encode())
    c.send(encoded_text)
    byterecv = c.recv(1024)
    decoded_text = cipher_suite.decrypt(byterecv)
    botresp = decoded_text.decode()
    if '://' in botresp:
        webbrowser.open(botresp)
        botresp = 'opening url'

    botchat(botresp)

def usertemp(txt):
    TextBox.insert(END, "User", 'user')
    TextBox.insert(END, "\t")
    TextBox.insert(END, txt,'userval')
    TextBox.tag_config('user', foreground='black',background="orange",font=("Helvetica", 18))
    TextBox.tag_config('userval', foreground='black',font=("Helvetica", 16))
    TextBox.insert(END, "\n")
    TextBox.yview(END)
    TextBox.pack()
    root.update()
    

def botchat(txt):
    TextBox.insert(END, "Bot ",'bot')
    TextBox.insert(END, "\t")
    TextBox.insert(END, txt,'botval')
    TextBox.insert(END, "\n")
    TextBox.tag_config('bot', foreground='black',background="sky blue",font=("Helvetica", 18)) 
    TextBox.tag_config('botval', foreground='black',font=("Helvetica", 16)) 
    TextBox.yview(END)
    TextBox.pack()
    root.update()
    talkToMe(txt)

def addevent(txt):
    usertemp(txt)
    botchat("Name of the event ?")
    eventname = othercmd()
    botchat("which date ?")
    eventdate = othercmd()
    botchat("what time ?")
    eventtime = othercmd()
    resp = createevent(eventname,eventdate,eventtime)
    talkToMe(resp)


TextBox.insert(END, "Bot ",'bot')
TextBox.insert(END, "\t")
TextBox.insert(END, "Hi im IPM, im ready to help you",'botval')
TextBox.insert(END, "\n")
TextBox.tag_config('bot', foreground='black',background="sky blue",font=("Helvetica", 18)) 
TextBox.tag_config('botval', foreground='black',font=("Helvetica", 16)) 
TextBox.yview(END)
TextBox.pack()
root.update()
talkToMe('Hi im IPM, im ready to help you')
IP = "127.0.0.1"
PORT = 1234
c = socket.socket()
c.connect((IP, PORT))
while True:
    cmd = myCommand()
    TextBox.insert(END, "\n")
    TextBox.yview(END)
    root.update()
root.mainloop()
