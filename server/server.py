from __future__ import print_function
import nltk
from nltk.stem.lancaster import LancasterStemmer
stemmer = LancasterStemmer()
import socket
from _thread import *
import threading 
import numpy
import tflearn
import tensorflow
import random
import json
import crayons
import os
import re
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import requests
import json
import pandas as pd
import time
import datetime
from cryptography.fernet import Fernet
import pickle

SCOPES = ['https://www.googleapis.com/auth/calendar']
key = 'GCOKn9PQow4ZimsU0aBGZZQHIi_lTq7o2UmxLpK-wfI='.encode()
bburl = 'http://localhost:7990/rest/'
bbprojects = 'api/1.0/projects/'
bbrepos = '/repos/'
bbuser = 'api/1.0/users'
confurl = 'http://localhost:8091/rest/'
confuser = 'api/user/current'
confspace = 'api/space'
confuserid = 'mgkamalesh7'
confpass = 'Mgkamal@290199'
jiraid = 'mgkamalesh7'
jirapass = 'Mgkamal@290199'
jiraurl = "http://localhost:8080/rest/"
jiraproj = "api/2/project"
jiraissue = "api/2/issue/createmeta"
cipher_suite = Fernet(key)

with open("trainset.json") as file:
    data = json.load(file)

with open("data.pickle", "rb") as f:
    words, labels, training, output = pickle.load(f)

tensorflow.reset_default_graph()

net = tflearn.input_data(shape=[None, len(training[0])])
net = tflearn.fully_connected(net, 8)
net = tflearn.fully_connected(net, 8)
net = tflearn.fully_connected(net, len(output[0]), activation="softmax")
net = tflearn.regression(net)

model = tflearn.DNN(net)

model.load("model.tflearn")

def bag_of_words(s, words):
    bag = [0 for _ in range(len(words))]

    s_words = nltk.word_tokenize(s)
    s_words = [stemmer.stem(word.lower()) for word in s_words]

    for se in s_words:
        for i, w in enumerate(words):
            if w == se:
                bag[i] = 1
            
    return numpy.array(bag)

def getevents():
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('calendar', 'v3', credentials=creds)
    now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
    print('Getting the upcoming 10 events')
    events_result = service.events().list(calendarId='primary', timeMin=now,
                                        maxResults=10, singleEvents=True,
                                        orderBy='startTime').execute()
    events = events_result.get('items', [])
    return events
    

def chat(inp):
    print(inp)
    if 'open' in inp:
        if 'project' in inp:
            reg_ex = re.search('open project (.+)', inp)
            if reg_ex:
                text = reg_ex.group(1)
                response = 'http://localhost:7990/projects/'+text.strip()
            else:
                response = 'no such project found'
        elif 'repository' in inp:
            reg_ex = re.search('open repository (.+)', inp)
            if reg_ex:
                text = reg_ex.group(1)
                response = 'http://localhost:7990/projects/DEMO/repos/'+text.strip()
            else:
                response = 'no such repository found'
        elif 'user' in inp:
            reg_ex = re.search('open user (.+)', inp)
            if reg_ex:
                text = reg_ex.group(1)
                response = 'http://localhost:7990/users/'+text.strip()
            else:
                response = 'no such user found'
        elif 'space' in inp:
            reg_ex = re.search('open space (.+)', inp)
            if reg_ex:
                text = reg_ex.group(1)
                url = confurl + confspace
                jresp = requests.get(url, auth=(confuserid, confpass))
                string1 = text.replace(' ','').lower()
                spaces = jresp.json()['results']
                size = len(spaces)
                for i in range(size):
                    if(spaces[i]['name'].replace(' ','').lower() == string1):
                        response = 'http://localhost:8091/display/'+spaces[i]['key']
            else:
                response = 'no such space found'
    else:
        results = model.predict([bag_of_words(inp, words)])
        results_index = numpy.argmax(results)
        tag = labels[results_index]

        for tg in data["intents"]:
            if tg['tag'] == tag:
                tag = tg['tag']
                resp = tg['responses']
                action = tg['action']
                print(tag)
                if tag == 'user':
                    url = bburl+bbuser
                    jresp = requests.get(url,auth=('kamalesh', 'Mgkamal@290199'))
                    size = len(jresp.json()['values'])
                    response = str(size)+" user found"
                    for i in range(size):
                        user = jresp.json()['values'][0]['name']
                        response = response + " " + user
                elif tag == 'bitproject':
                    url = bburl+bbprojects
                    jresp = requests.get(url,auth=('kamalesh', 'Mgkamal@290199'))
                    size = len(jresp.json()['values'])
                    response = str(size)+" project found"
                    for i in range(size):
                        proj = jresp.json()['values'][0]['name']
                        response = response + " " + proj
                elif tag == 'jiraproject':
                    url = jiraurl+jiraproj
                    jresp = requests.get(url,auth=(jiraid, jirapass))
                    size = len(jresp.json())
                    response = str(size)+" project found"
                    for i in range(size):
                       proj = jresp.json()[i]['name']
                       response = response + " " + proj
                    #print(response)
                elif tag == 'issue':
                    url = jiraurl+jiraissue
                    jresp = requests.get(url,auth=('mgkamalesh7', 'Mgkamal@290199'))
                    size = len(jresp.json())
                    response = str(size)+" issue found"

                elif tag == 'repository':
                    url = bburl+bbprojects
                    jresp = requests.get(url,auth=('kamalesh', 'Mgkamal@290199'))
                    size = len(jresp.json()['values'])
                    for i in range(size):
                        proj = jresp.json()['values'][0]['name']
                        response = "project "+ proj + " contains "
                        url = bburl+bbprojects+proj+bbrepos
                        rjresp = requests.get(url,auth=('kamalesh', 'Mgkamal@290199'))
                        rsize = len(rjresp.json()['values'])
                        response = response + str(rsize) + " repository"
                        for i in range(rsize):
                            repo = rjresp.json()['values'][i]['name']
                            response = response + " " + repo
                elif tag == 'getevent':
                    events = getevents()
                    if not events:
                        response  = 'No upcoming events found'
                    else:
                        response  = "There are "+str(len(events))+" upcoming events. "
                        i = 1
                        for event in events:
                            start = event['start'].get('dateTime', event['start'].get('date'))
                            month = start[5:]
                            month = month[:-18]
                            monthstr = datetime.date(1900, int(month), 1).strftime('%B')
                            response = response + " " + str(i) + " " + event['summary'] + " on " + monthstr + " " + start[8:10]+ " " +start[0:4] + ". "
                            i = i+1
                elif tag == 'space':
                    url = confurl + confspace
                    jresp = requests.get(url, auth=(confuserid, confpass))
                    spaces = jresp.json()['results']
                    size = len(spaces)
                    response = str(size) + " " + "spaces found." + " "
                    for i in range(size):
                        response = response + " " +str(i+1) + " " + spaces[i]['name'] + ". "
                else:
                    response = random.choice(resp)

    print(crayons.green("Bot: "+response))
    return response

#def executeresp(resp):

def threaded(c): 
    while True:
        byterecv = c.recv(1024)
        decoded_text = cipher_suite.decrypt(byterecv)
        data = decoded_text.decode()
        if not data: 
            print('Bye')   
            break
        resp = chat(data)
        encoded_text = cipher_suite.encrypt(resp.encode())
        c.send(encoded_text)
    c.close() 


host = "127.0.0.1" 
port = 1234
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
s.bind((host, port)) 
print("socket binded to port", port) 
s.listen(5)
while True:
    c, addr = s.accept()  
    print('Connected to :', addr) 
    start_new_thread(threaded, (c,)) 
s.close() 

